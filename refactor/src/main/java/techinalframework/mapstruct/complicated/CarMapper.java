package techinalframework.mapstruct.complicated;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CarMapper {
    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    @Mapping(source = "numberOfSeats", target = "seatCount")
    @Mapping(source = "wheel", target = "wheelDTO")
    @Mapping(source = "ignoredProperty", target = "ignoredProperty", ignore = true)
    @Mapping(source = "feeAmounts", target = "feeAmounts", qualifiedByName = "integerListToStringListCustom")
    @Mapping(target = "customizedField", expression = "java(Tools.convertCustom(car.getCustomField()))")
    CarDTO carToCarDTO(Car car);

    @InheritInverseConfiguration
    @Mapping(source = "feeAmounts", target = "feeAmounts", qualifiedByName = "stringListToIntegerListCustom")
    @Mapping(target = "customField", expression = "java(Tools.convertCustomBack(carDTO.getCustomizedField()))")
    Car carDTOToCar(CarDTO carDTO);

    @Mapping(source = "type", target = "typeName")
    WheelDTO wheelToWheelDTO(Wheel wheel);

    @InheritInverseConfiguration
    Wheel wheelDTOToWheel(WheelDTO wheelDTO);

    @Named("integerListToStringListCustom")
    @IterableMapping(numberFormat = "#.00")
    List<String> integerListToStringListCustom(List<Integer> feeAmounts);

    @Named("stringListToIntegerListCustom")
    @IterableMapping(numberFormat = "#.00")
    List<Integer> stringListToIntegerListCustom(List<String> feeAmounts);
}
