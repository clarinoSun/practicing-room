package techinalframework.mapstruct.complicated;

import java.util.List;

public class CarDTO {

    private String make;
    private int seatCount;
    private String type;

    private WheelDTO wheelDTO;
    private String ignoredProperty;

    private List<String> feeAmounts;


    private String customizedField;

    public CarDTO(String make, int seatCount, String type) {
        this.make = make;
        this.seatCount = seatCount;
        this.type = type;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMake() {
        return make;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public String getType() {
        return type;
    }

    public WheelDTO getWheelDTO() {
        return wheelDTO;
    }

    public void setWheelDTO(WheelDTO wheelDTO) {
        this.wheelDTO = wheelDTO;
    }

    public String getIgnoredProperty() {
        return ignoredProperty;
    }

    public void setIgnoredProperty(String ignoredProperty) {
        this.ignoredProperty = ignoredProperty;
    }

    public List<String> getFeeAmounts() {
        return feeAmounts;
    }

    public void setFeeAmounts(List<String> feeAmounts) {
        this.feeAmounts = feeAmounts;
    }

    public String getCustomizedField() {
        return customizedField;
    }

    public void setCustomizedField(String customizedField) {
        this.customizedField = customizedField;
    }
}