package techinalframework.mapstruct.complicated;

import java.util.List;

public class Car {

    private String make;
    private int numberOfSeats;
    private CarType type;

    private Wheel wheel;

    private String ignoredProperty;

    private List<Integer> feeAmounts;

    private String customField;

    public Car(String make, int numberOfSeats, CarType type) {
        this.make = make;
        this.numberOfSeats = numberOfSeats;
        this.type = type;
    }

    public String getMake() {
        return make;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public CarType getType() {
        return type;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public String getIgnoredProperty() {
        return ignoredProperty;
    }

    public void setIgnoredProperty(String ignoredProperty) {
        this.ignoredProperty = ignoredProperty;
    }

    public List<Integer> getFeeAmounts() {
        return feeAmounts;
    }

    public void setFeeAmounts(List<Integer> feeAmounts) {
        this.feeAmounts = feeAmounts;
    }

    public String getCustomField() {
        return customField;
    }

    public void setCustomField(String customField) {
        this.customField = customField;
    }
}