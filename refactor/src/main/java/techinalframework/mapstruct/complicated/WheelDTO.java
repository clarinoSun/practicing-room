package techinalframework.mapstruct.complicated;

public class WheelDTO {
    private String typeName;

    public WheelDTO(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
