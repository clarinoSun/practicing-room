package techinalframework.mapstruct.easiest;

public class CarDTO {

    private String brand;
    private int seatCount;
    private String type;

    public CarDTO(String brand, int seatCount, String type) {
        this.brand = brand;
        this.seatCount = seatCount;
        this.type = type;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public String getType() {
        return type;
    }

}