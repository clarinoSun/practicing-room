package techinalframework.mapstruct.easiest;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import techinalframework.mapstruct.complicated.Wheel;
import techinalframework.mapstruct.complicated.WheelDTO;

import java.util.List;

@Mapper
public interface CarMapper {
    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    @Mapping(source = "numberOfSeats", target = "seatCount")
    CarDTO carToCarDTO(Car car);


}
