package techinalframework.mapstruct.easiest;

public class Car {

    private String brand;
    private int numberOfSeats;
    private CarType type;

    public Car(String brand, int numberOfSeats, CarType type) {
        this.brand = brand;
        this.numberOfSeats = numberOfSeats;
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public CarType getType() {
        return type;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setType(CarType type) {
        this.type = type;
    }

}