package tdd.kawaii;

public class KawaiiGame {
    public String calc(int input) {
        if (input < 1 || input > 200) {
            throw new IllegalArgumentException();
        }
        String inputString = String.valueOf(input);
        if (inputString.contains("3")) {
            return "ka";
        }
        String result = "";

        if (input % 3 == 0) {
            result += "ka";
        }
        if (input % 5 == 0) {
            result += "wa";
        }
        if (input % 7 == 0) {
            result += "ii";
        }
        return result.isEmpty() ? inputString : result;
    }

}