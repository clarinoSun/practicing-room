package refactor.replaceparameterwithexplicitmethods;

public class Employee {
    private int height;
    private int age;

    public void setHeight(int height) {
        this.height = height;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static Employee createEngineer() {
        return new Engineer();
    }

    public static Employee createSalesman() {
        return new Salesman();
    }

    public static Employee createManager() {
        return new Manager();
    }
}
