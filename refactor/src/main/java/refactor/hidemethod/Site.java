package refactor.hidemethod;

public class Site {
    private String name;
    private String openTime;

    private String stateCustomer(String gender, String name) {
        return "gender:" + ("M".equals(gender) ? "男" : "女");
    }

    private String stateSite(String name, String openTime) {
        return "name:" + name + ",openTime:" + openTime;
    }

    public void modify(String name, String openTime) {
        this.name = name;
        this.openTime = openTime;
    }

    public String state() {
        return stateCustomer("M", "tom") + stateSite(name, openTime);
    }
}
