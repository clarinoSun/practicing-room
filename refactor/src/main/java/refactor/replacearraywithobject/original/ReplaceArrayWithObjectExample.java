package refactor.replacearraywithobject.original;

public class ReplaceArrayWithObjectExample {
    public String stateTeamPerformance() {
        String[] row = new String[2];
        row[0] = "Liverpool";
        row[1] = "15";
        String name = row[0];
        int wins = Integer.parseInt(row[1]);
        return "name:" + name + ",wins:" + wins;
    }
}
