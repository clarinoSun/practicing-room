package refactor.replacearraywithobject;

public class Performance {
    private String name;
    private String wins;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    public String getWins() {
        return this.wins;
    }
}
