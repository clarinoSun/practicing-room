package refactor.replacearraywithobject;

public class ReplaceArrayWithObjectExample {
    public String stateTeamPerformance() {
        Performance row = new Performance();
        row.setName("Liverpool");
        row.setWins("15");
        String name = row.getName();
        int wins = Integer.parseInt(row.getWins());
        return "name:" + name + ",wins:" + wins;
    }

}
