package refactor.introduceexplainingvariable;

public class IntroduceExplainingVariableExample {

    private String platform;
    private String browser;
    private int resize;
    private int quantity;
    private int itemPrice;

    private void introduceVariableConditionalMethod() {
        final boolean isMacOs = platform.toUpperCase().indexOf("MAC") > -1;
        final boolean isIEBrowser = browser.toUpperCase().indexOf("IE") > -1;
        final boolean isResized = resize > 0;
        if (isMacOs &&
                isIEBrowser && isInitialized() && isResized) {
            //do something
        }
    }

    private double calcPrice() {
        return calcBasePrice() - calcQuantityDiscount() + calcShipping();
    }

    private double calcShipping() {
        return Math.min(quantity * itemPrice * 0.1, 100.0);
    }

    private double calcQuantityDiscount() {
        return Math.max(0, quantity - 500) * itemPrice * 0.05;
    }

    private int calcBasePrice() {
        return quantity * itemPrice;
    }

    private boolean isInitialized() {
        return false;
    }
}