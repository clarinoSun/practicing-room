package refactor.removecontrolflag;

public class RemoveControlFlagExample {
    public boolean checkSecurity(String[] people) {
        boolean sendAlert = false;
        for (int i = 0; i < people.length; i++) {
            if (people[i].equals("sam")) {
                sendAlert = sendAlert();
                break;
            }
            if (people[i].equals("tom")) {
                sendAlert = sendAlert();
                break;
            }
        }
        return sendAlert;
    }

    public String findMiscreant(String[] people) {
        for (int i = 0; i < people.length; i++) {
            if (people[i].equals("sam")) {
                sendAlert();
                return "sam";
            }
            if (people[i].equals("tom")) {
                sendAlert();
                return "tom";
            }
        }
        return "";
    }

    public boolean sendAlert() {
        return true;
    }

}
