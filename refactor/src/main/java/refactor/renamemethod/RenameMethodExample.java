package refactor.renamemethod;

public class RenameMethodExample {
    private CustomerDao customerDao;

    public RenameMethodExample(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public String queryHolder(String holderName) {
        return customerDao.query(holderName);
    }
}
