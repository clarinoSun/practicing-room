package refactor.preservewholeobject.original;

public class Room {
    private TempRange daysTempRange;

    public boolean withinPlan(HeatingPlan plan) {
        int low = getDaysTempRange().getLow();
        int high = getDaysTempRange().getHigh();
        return plan.withinRange(low, high);
    }

    public TempRange getDaysTempRange() {
        return daysTempRange;
    }

    public void setDaysTempRange(TempRange daysTempRange) {
        this.daysTempRange = daysTempRange;
    }
}
