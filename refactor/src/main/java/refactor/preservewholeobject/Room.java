package refactor.preservewholeobject;

public class Room {
    private TempRange daysTempRange;

    public boolean withinPlan(HeatingPlan plan) {
        return plan.withinRange(getDaysTempRange());
    }

    public TempRange getDaysTempRange() {
        return daysTempRange;
    }

    public void setDaysTempRange(TempRange daysTempRange) {
        this.daysTempRange = daysTempRange;
    }
}
