package refactor.preservewholeobject;

public class HeatingPlan {
    private TempRange range;

    public HeatingPlan(TempRange range) {
        this.range = range;
    }

    public boolean withinRange(TempRange tempRange) {
        return range.includes(tempRange);
    }

}
