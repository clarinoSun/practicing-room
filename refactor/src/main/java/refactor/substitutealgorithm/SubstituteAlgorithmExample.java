package refactor.substitutealgorithm;


import org.assertj.core.util.Lists;

import java.util.ArrayList;

public class SubstituteAlgorithmExample {
    public String findPerson(String[] people) {
        final ArrayList<String> candidates = Lists.newArrayList("Tom", "Sam", "Tim", "Bob");
        for (int i = 0; i < people.length; i++) {
            if (candidates.contains(people[i])) {
                return people[i];
            }
        }
        return "";
    }
}