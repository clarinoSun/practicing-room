package refactor.substitutealgorithm;

public class SubstituteAlgorithmExampleOriginal {
    public String findPerson(String[] people) {
        for (int i = 0; i < people.length; i++) {
            if (people[i].equals("Tom")) {
                return "Tom";
            }
            if (people[i].equals("Sam")) {
                return "Sam";
            }
            if (people[i].equals("Tim")) {
                return "Tim";
            }
        }
        return "";
    }
}