package refactor.introduceforeignmethod;

import java.util.Date;

public class IntroduceForeignMethodExample {
    public double calcPrice(Date previousDate) {
        final Date newStart = getNextDay(previousDate);
        if (newStart.after(new Date(1990, 10, 8))) {
            return 20;
        } else {
            return 10;
        }
    }

    private static Date getNextDay(Date previousDate) {
        // foreign method,should be on date
        final Date newStart = new Date(previousDate.getYear(), previousDate.getMonth(), previousDate.getDate() + 1);
        return newStart;
    }
}