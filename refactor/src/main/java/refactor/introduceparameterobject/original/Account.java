package refactor.introduceparameterobject.original;

import java.util.Date;
import java.util.List;

public class Account {
    private List<Entry> entries;

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public double getFlowBetween(Date start, Date end) {
        double result = 0;
        for (Entry entry : entries) {
            if (entry.getChargeDate().equals(start) || entry.getChargeDate().equals(end) ||
                    entry.getChargeDate().after(start) && entry.getChargeDate().before(end)
            ) {
                result += entry.getValue();
            }
        }
        return result;
    }
}
