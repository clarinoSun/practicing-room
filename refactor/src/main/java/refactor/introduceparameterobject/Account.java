package refactor.introduceparameterobject;

import java.util.List;

public class Account {
    private List<Entry> entries;

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public double getFlowBetween(DateRange dateRange) {
        return entries.stream().filter(entry -> dateRange.includes(entry.getChargeDate())).
                mapToDouble(Entry::getValue).sum();
    }

}
