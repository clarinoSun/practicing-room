package refactor.movemethod;

public class Account {
    private AccountType type;
    private int daysOverdrawn;

    public Account(AccountType type, int daysOverdrawn) {
        this.type = type;
        this.daysOverdrawn = daysOverdrawn;
    }

    public double calcOverdraftCharge() {
        return type.calcOverdraftCharge(daysOverdrawn);
    }

    public double calcBankCharge() {
        double result = 4.5;
        if (daysOverdrawn > 0) {
            result += calcOverdraftCharge();
        }
        return result;
    }
}