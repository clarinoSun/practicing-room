package refactor.movemethod;

public class AccountType {
    public static final int ACCOUNT_TYPE_PREMIUM = 1;
    public static final int ACCOUNT_TYPE_SENIOR = 2;
    private int accountType;

    public AccountType(int accountType) {
        this.accountType = accountType;
    }

    public boolean isPremium() {
        return ACCOUNT_TYPE_PREMIUM == accountType;
    }

    public double calcOverdraftCharge(int daysOverdrawn) {
        if (isPremium()) {
            double result = 10;
            if (daysOverdrawn > 7) {
                result += (daysOverdrawn - 7) * 0.85;
            }
            return result;
        } else {
            return daysOverdrawn * 1.75;
        }
    }
}