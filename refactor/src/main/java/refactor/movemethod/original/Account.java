package refactor.movemethod.original;

public class Account {
    private AccountType type;
    private int daysOverdrawn;

    public Account(AccountType type, int daysOverdrawn) {
        this.type = type;
        this.daysOverdrawn = daysOverdrawn;
    }

    public double calcOverdraftCharge() {
        if (type.isPremium()) {
            double result = 10;
            if (daysOverdrawn > 7) {
                result += (daysOverdrawn - 7) * 0.85;
            }
            return result;
        } else {
            return daysOverdrawn * 1.75;
        }
    }

    public double calcBankCharge() {
        double result = 4.5;
        if (daysOverdrawn > 0) {
            result += calcOverdraftCharge();
        }
        return result;
    }
}