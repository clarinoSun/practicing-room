package refactor.movemethod.original;

public class AccountType {
    public static final int ACCOUNT_TYPE_PREMIUM = 1;
    public static final int ACCOUNT_TYPE_SENIOR = 2;
    private int accountType;

    public AccountType(int accountType) {
        this.accountType = accountType;
    }

    public boolean isPremium() {
        return ACCOUNT_TYPE_PREMIUM == accountType;
    }

}