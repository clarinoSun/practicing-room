package refactor.movemethod.result;

public class Account {
    private AccountType type;
    private int daysOverdrawn;

    public double calcBankCharge() {
        double result = 4.5;
        if (daysOverdrawn > 0) {
            result += type.calcOverdraftCharge(daysOverdrawn);
        }
        return result;
    }
}