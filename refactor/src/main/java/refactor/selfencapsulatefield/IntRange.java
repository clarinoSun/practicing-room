package refactor.selfencapsulatefield;

public class IntRange {
    private int low;
    private int high;

    public IntRange(int low, int high) {
        this.low = low;
        this.high = high;
    }

    public boolean includes(int arg) {
        return arg >= getLow() && arg <= getHigh();
    }

    public void grow(int factor) {
        setHigh(getHigh() * factor);
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }
}
