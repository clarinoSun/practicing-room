package refactor.addparameter;

public class Site {
    @Deprecated
    public String stateCustomer(String gender) {
        return stateCustomerRefactor(gender, null);
    }

    public String stateCustomerRefactor(String gender, String name) {
        return "gender:" + ("M".equals(gender) ? "男" : "女");
    }

    public String state() {
        return stateCustomerRefactor("M", "tom");
    }
}
