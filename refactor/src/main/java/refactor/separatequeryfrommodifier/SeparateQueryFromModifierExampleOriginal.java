package refactor.separatequeryfrommodifier;

public class SeparateQueryFromModifierExampleOriginal {

    public String findMiscreant(String[] people) {
        for (int i = 0; i < people.length; i++) {
            if (people[i].equals("sam")) {
                sendAlert();
                return "sam";
            }
            if (people[i].equals("tom")) {
                sendAlert();
                return "tom";
            }
        }
        return "";
    }

    public void sendAlert() {
        // do something
    }
}
