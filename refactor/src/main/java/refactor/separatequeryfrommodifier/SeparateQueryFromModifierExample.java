package refactor.separatequeryfrommodifier;

public class SeparateQueryFromModifierExample {
    public void findMiscreant(String[] people) {
        if (!"".equals(findPeople(people))) {
            sendAlert();
        }
    }

    public String findPeople(String[] people) {
        for (int i = 0; i < people.length; i++) {
            if (people[i].equals("sam")) {
                return "sam";
            }
            if (people[i].equals("tom")) {
                return "tom";
            }
        }
        return "";
    }

    private void sendAlert() {
        // do something
    }
}
