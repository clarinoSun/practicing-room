package refactor.introducelocalextension.wrapping;

import java.util.Date;

public class DateExtension {

    private Date date;

    public DateExtension(Date date) {
        this.date = date;
    }

    Date getNextDay() {
        final Date newStart = new Date(this.date.getYear(), this.date.getMonth(), this.date.getDate() + 1);
        return newStart;
    }
}