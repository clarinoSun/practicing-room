package refactor.introducelocalextension.wrapping;

import java.util.Date;

public class IntroduceLocalExtensionExample {
    public double calcPrice(Date previousDate) {
        final Date newStart = new DateExtension(previousDate).getNextDay();
        if (newStart.after(new Date(1990, 10, 8))) {
            return 20;
        } else {
            return 10;
        }
    }

}