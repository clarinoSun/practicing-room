package refactor.introducelocalextension.subclassing;

import java.util.Date;

public class DateSub extends Date {
    public DateSub(int year, int month, int date) {
        super(year, month, date);
    }

    Date getNextDay() {
        final Date newStart = new Date(getYear(), getMonth(), getDate() + 1);
        return newStart;
    }
}