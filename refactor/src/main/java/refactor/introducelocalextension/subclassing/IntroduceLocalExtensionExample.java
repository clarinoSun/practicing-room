package refactor.introducelocalextension.subclassing;

import java.util.Date;

public class IntroduceLocalExtensionExample {
    public double calcPrice(DateSub previousDate) {
        final Date newStart = previousDate.getNextDay();
        if (newStart.after(new Date(1990, 10, 8))) {
            return 20;
        } else {
            return 10;
        }
    }

}