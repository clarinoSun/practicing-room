package refactor.changevaluetoreference;

import java.util.List;

public class Client {
    public long countOrdersForCustomer(List<Order> orders, String customer) {
        return orders.stream().filter(order -> customer.equals(order.getCustomerName())).count();
    }

    public long countOrdersForCustomer(List<Order> orders, Customer customer) {
        return orders.stream().filter(order -> customer == order.getCustomer()).count();
    }
}
