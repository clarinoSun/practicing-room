package refactor.changevaluetoreference;

import org.assertj.core.util.Lists;

import java.util.List;

public class Customer {
    private String name;
    private static List<Customer> instances;

    private Customer(String name) {
        this.name = name;
    }

    public static Customer getNamed(String name) {
        if (instances == null) {
            loadCustomers();
        }
        return instances.stream().filter(instance -> name.equals(instance.getName())).findFirst().orElse(null);
    }

    private static void loadCustomers() {
        instances = Lists.newArrayList(new Customer("Tom"), new Customer("Sam"));
    }

    public String getName() {
        return name;
    }
}
