package refactor.changevaluetoreference.original;

import java.util.List;

public class Client {
    public long countOrdersForCustomer(List<Order> orders, String customer) {
        return orders.stream().filter(order -> customer.equals(order.getCustomerName())).count();
    }
}
