package refactor.changevaluetoreference.original;

public class Order {
    private Customer customer;

    public Order(String customerName) {
        customer = new Customer(customerName);
    }

    public String getCustomerName() {
        return customer.getName();
    }
}
