package refactor.changevaluetoreference.result;

import org.assertj.core.util.Lists;

import java.util.List;

public class Customer {
    private String name;
    private static List<Customer> instances;

    private Customer(String name) {
        this.name = name;
    }

    public static void loadCustomers() {
        instances = Lists.newArrayList(new Customer("Sam"), new Customer("Tom"));
    }

    public static Customer create(String name) {
        if (instances == null) {
            loadCustomers();
        }
        return instances.stream().filter(instance -> name.equals(instance.getName())).findFirst().orElse(null);
    }

    public String getName() {
        return name;
    }
}
