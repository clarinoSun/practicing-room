package refactor.changevaluetoreference;

public class Order {
    private Customer customer;

    public Order(String customerName) {
        customer = Customer.getNamed(customerName);
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public Customer getCustomer() {
        return customer;
    }
}
