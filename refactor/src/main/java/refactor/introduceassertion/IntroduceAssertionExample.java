package refactor.introduceassertion;

import static org.assertj.core.api.Assertions.assertThat;

public class IntroduceAssertionExample {
    private ExpenseLimit expenseLimit;
    private Project primaryProject;

    public double calcExpenseLimit() {
        assertThat(expenseLimit != null || primaryProject != null).isTrue();
        return expenseLimit != null ? expenseLimit.getAmount() : primaryProject.getMemberExpenseLimit();
    }
}
