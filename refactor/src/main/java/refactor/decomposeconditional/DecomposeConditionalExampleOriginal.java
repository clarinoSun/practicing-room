package refactor.decomposeconditional;

import java.util.Date;

public class DecomposeConditionalExampleOriginal {
    private static final Date SUMMER_START = new Date(2022, 6, 1);
    private static final Date SUMMER_END = new Date(2022, 9, 1);

    public double calcCharge(Date date, int quantity, double winterRate, double summerRate, double winterServiceCharge) {
        double charge;
        if (date.before(SUMMER_START) || date.after(SUMMER_END)) {
            charge = quantity * winterRate + winterServiceCharge;
        } else {
            charge = quantity * summerRate;
        }
        return charge;
    }

}
