package refactor.decomposeconditional;

import java.util.Date;

public class DecomposeConditionalExample {
    private static final Date SUMMER_START = new Date(2022, 6, 1);
    private static final Date SUMMER_END = new Date(2022, 9, 1);

    public double calcCharge(Date date, int quantity, double winterRate, double summerRate, double winterServiceCharge) {
        double charge;
        if (notSummer(date)) {
            charge = calcWinterCharge(quantity, winterRate, winterServiceCharge);
        } else {
            charge = calcSummerCharge(quantity, summerRate);
        }
        return charge;
    }

    private double calcSummerCharge(int quantity, double summerRate) {
        double charge;
        charge = quantity * summerRate;
        return charge;
    }

    private double calcWinterCharge(int quantity, double winterRate, double winterServiceCharge) {
        double charge;
        charge = quantity * winterRate + winterServiceCharge;
        return charge;
    }

    private boolean notSummer(Date date) {
        return date.before(SUMMER_START) || date.after(SUMMER_END);
    }
}
