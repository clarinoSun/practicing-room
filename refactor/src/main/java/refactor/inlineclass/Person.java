package refactor.inlineclass;

public class Person {
    private String name;
    private String areaCode;
    private String number;

    public Person(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return "(" + areaCode + ")" + number;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}