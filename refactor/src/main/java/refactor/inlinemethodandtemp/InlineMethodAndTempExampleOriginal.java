package refactor.inlinemethodandtemp;

import java.util.Hashtable;

public class InlineMethodAndTempExampleOriginal {
    private String _name;
    private Hashtable orders;

    void printOwing() {
        double outstanding = calcPreviousOutstanding() + calcAfterOutstanding();
        //print banner
        System.out.println("************************************");
        System.out.println("***********Customer Owes***********");
        System.out.println("************************************");
        //calculate outstanding
        outstanding = getOutstanding(outstanding);
        //print details
        System.out.println("name:" + _name);
        System.out.println("amount" + outstanding);
    }

    private double getOutstanding(double outstanding) {
        while (orders.elements().hasMoreElements()) {
            Order each = (Order) orders.elements().nextElement();
            outstanding += each.getAmount();
        }
        return outstanding;
    }

    double calcPreviousOutstanding() {
        return getCurrentOutstanding() > 0 ? 2 : 1;
    }

    double calcAfterOutstanding() {
        return getCurrentOutstanding() > 0 ? 20 : 10;
    }

    double getCurrentOutstanding() {
        return 10;
    }
}