package refactor.replaceparameterwithmethods;

public class Order {
    private int quantity;
    private int itemPrice;

    public Order(int quantity, int itemPrice) {
        this.quantity = quantity;
        this.itemPrice = itemPrice;
    }

    public double calcPrice() {
        if (calcDiscountLevel() == 2) {
            return calcBasePrice() * 0.1;
        } else {
            return calcBasePrice() * 0.05;
        }
    }

    private int calcDiscountLevel() {
        int discountLevel;
        if (quantity > 100) {
            discountLevel = 2;
        } else {
            discountLevel = 1;
        }
        return discountLevel;
    }

    private int calcBasePrice() {
        return quantity * itemPrice;
    }

}
