package refactor.movefield;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Account {
    private AccountType type;

    public Account(AccountType type) {
        this.type = type;
    }

    public void setInterestRate(double interestRate) {
        type.setInterestRate(interestRate);
    }

    public BigDecimal calcInterest(double amount, double days) {
        if (type.isPremium()) {
            return BigDecimal.valueOf(type.getInterestRate() * amount * days / 365).setScale(2, RoundingMode.HALF_UP);
        } else {
            return BigDecimal.valueOf(type.getInterestRate() * amount * (days / 30) / 12).setScale(2, RoundingMode.HALF_UP);
        }
    }

}