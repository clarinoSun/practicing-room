package refactor.movefield.original;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Account {
    public static final int ACCOUNT_TYPE_PREMIUM = 1;
    public static final int ACCOUNT_TYPE_SENIOR = 2;
    private AccountType type;
    private double interestRate;

    public Account(AccountType type) {
        this.type = type;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal calcInterest(double amount, double days) {
        if (type.isPremium()) {
            return BigDecimal.valueOf(interestRate * amount * days / 365).setScale(2, RoundingMode.HALF_UP);
        } else {
            return BigDecimal.valueOf(interestRate * amount * (days / 30) / 12).setScale(2, RoundingMode.HALF_UP);
        }
    }

}