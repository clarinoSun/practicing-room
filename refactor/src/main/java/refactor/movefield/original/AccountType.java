package refactor.movefield.original;

public class AccountType {
    private int accountType;

    public AccountType(int accountType) {
        this.accountType = accountType;
    }

    public boolean isPremium() {
        return Account.ACCOUNT_TYPE_PREMIUM == accountType;
    }

}