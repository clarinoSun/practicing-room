package refactor.movefield;

public class AccountType {
    public static final int ACCOUNT_TYPE_PREMIUM = 1;
    public static final int ACCOUNT_TYPE_SENIOR = 2;
    private int accountType;
    private double interestRate;

    public AccountType(int accountType) {
        this.accountType = accountType;
    }

    public boolean isPremium() {
        return ACCOUNT_TYPE_PREMIUM == accountType;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }
}