package refactor.extractmethod;

import java.util.Hashtable;

public class ExtractMethodExample {
    private String _name;
    private Hashtable orders;

    void printOwing() {
        printBanner();
        double outstanding = calculateOutstanding();
        printDetails(outstanding);
    }

    private void printDetails(double outstanding) {
        System.out.println("name:" + _name);
        System.out.println("amount" + outstanding);
    }

    private double calculateOutstanding() {
        double outstanding = 0.0;
        while (orders.elements().hasMoreElements()) {
            Order each = (Order) orders.elements().nextElement();
            outstanding += each.getAmount();
        }
        return outstanding;
    }

    private void printBanner() {
        System.out.println("************************************");
        System.out.println("***********Customer Owes***********");
        System.out.println("************************************");
    }
}