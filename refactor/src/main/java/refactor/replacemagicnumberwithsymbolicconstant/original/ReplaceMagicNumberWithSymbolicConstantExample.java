package refactor.replacemagicnumberwithsymbolicconstant.original;

public class ReplaceMagicNumberWithSymbolicConstantExample {
    double calcPotentialEnergy(double mass, double height) {
        return mass * 9.81 * height;
    }
}
