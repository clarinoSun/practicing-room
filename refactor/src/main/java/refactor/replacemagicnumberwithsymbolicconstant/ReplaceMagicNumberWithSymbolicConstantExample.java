package refactor.replacemagicnumberwithsymbolicconstant;

public class ReplaceMagicNumberWithSymbolicConstantExample {

    public static final double GRAVITATIONAL_CONSTANT = 9.81;

    double calcPotentialEnergy(double mass, double height) {
        return mass * GRAVITATIONAL_CONSTANT * height;
    }
}
