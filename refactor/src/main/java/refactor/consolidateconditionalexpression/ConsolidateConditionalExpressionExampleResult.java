package refactor.consolidateconditionalexpression;

public class ConsolidateConditionalExpressionExampleResult {
    public double calcDisabilityAmount(int age, int monthsDisabled, boolean isPartTime) {
        if (isNotEligibleForDisability(age, monthsDisabled, isPartTime)) {
            return 0;
        }
        return monthsDisabled > 6 && age > 60 ? 30000 : 10000;
    }

    private boolean isNotEligibleForDisability(int age, int monthsDisabled, boolean isPartTime) {
        return age < 2 || monthsDisabled > 12 || isPartTime;
    }
}
