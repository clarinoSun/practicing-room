package refactor.consolidateconditionalexpression;

public class ConsolidateConditionalExpressionExampleOriginal {
    public double calcDisabilityAmount(int age, int monthsDisabled, boolean isPartTime) {
        if (age < 2) {
            return 0;
        }
        if (monthsDisabled > 12) {
            return 0;
        }
        if (isPartTime) {
            return 0;
        }
        if (monthsDisabled > 6) {
            if (age > 60) {
                return 30000;
            }
        }
        return 10000;
    }
}
