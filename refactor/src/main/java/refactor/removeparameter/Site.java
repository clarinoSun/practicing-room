package refactor.removeparameter;

public class Site {
    @Deprecated
    public String stateCustomer(String gender) {
        return stateCustomerRefactor(gender);
    }

    public String stateCustomerRefactor(String gender) {
        return "gender:" + ("M".equals(gender) ? "男" : "女");
    }

    public String state() {
        return stateCustomerRefactor("M");
    }
}
