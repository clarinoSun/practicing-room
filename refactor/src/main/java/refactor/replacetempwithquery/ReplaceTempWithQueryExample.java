package refactor.replacetempwithquery;

public class ReplaceTempWithQueryExample {
    private int quantity;
    private int itemPrice;

    public double calcPrice() {
        final int basePrice = calcBasePrice();
        return basePrice * calcDiscountFactor(basePrice);
    }

    private double calcDiscountFactor(int basePrice) {
        if (basePrice > 1000) {
            return 0.95;
        } else {
            return 0.98;
        }
    }

    private int calcBasePrice() {
        return quantity * itemPrice;
    }
}