package refactor.replacetempwithquery;

public class ReplaceTempWithQueryExampleResult {
    private int quantity;
    private int itemPrice;

    public double calcPrice() {
        return calcBasePrice() * calcDiscountFactor();
    }

    private double calcDiscountFactor() {
        if (calcBasePrice() > 1000) {
            return 0.95;
        } else {
            return 0.98;
        }
    }

    private int calcBasePrice() {
        return quantity * itemPrice;
    }
}