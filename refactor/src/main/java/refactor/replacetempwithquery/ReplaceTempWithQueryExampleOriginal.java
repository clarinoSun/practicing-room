package refactor.replacetempwithquery;

public class ReplaceTempWithQueryExampleOriginal {
    private int quantity;
    private int itemPrice;

    public double calcPrice() {
        int basePrice = quantity * itemPrice;
        double discountFactor;
        if (basePrice > 1000) {
            discountFactor = 0.95;
        } else {
            discountFactor = 0.98;
        }
        return basePrice * discountFactor;
    }
}