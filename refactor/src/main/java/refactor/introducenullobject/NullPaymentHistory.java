package refactor.introducenullobject;

public class NullPaymentHistory extends PaymentHistory {
    @Override
    public int getWeeksDelinquent() {
        return 0;
    }
}
