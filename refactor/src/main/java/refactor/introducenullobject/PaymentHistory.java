package refactor.introducenullobject;

public class PaymentHistory {
    private int weeksDelinquent;

    public PaymentHistory() {
    }

    public PaymentHistory(int weeksDelinquent) {
        this.weeksDelinquent = weeksDelinquent;
    }

    public int getWeeksDelinquent() {
        return weeksDelinquent;
    }

}
