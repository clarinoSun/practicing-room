package refactor.introducenullobject;


public class Site {
    private Customer customer;

    public String state() {
        Customer customer = getCustomer();
        BillingPlan plan = customer.getPlan();
        String customerName = customer.getName();
        int weeksDelinquent = customer.getHistory().getWeeksDelinquent();
        return "name:" + customerName + ",plan:" + plan + ",weeksDelinquent:" + weeksDelinquent;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        if (customer == null) {
            return new NullCustomer();
        }
        return customer;
    }
}
