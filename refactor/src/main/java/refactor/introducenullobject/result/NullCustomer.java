package refactor.introducenullobject.result;

public class NullCustomer extends Customer {
    @Override
    public String getName() {
        return "occupant";
    }

    @Override
    public BillingPlan getPlan() {
        return BillingPlan.getBasic();
    }

    @Override
    public PaymentHistory getHistory() {
        return new NullPaymentHistory();
    }
}
