package refactor.introducenullobject.result;

public class Site {
    private Customer customer;

    public String state() {
        Customer customer = getCustomer();
        String customerName = customer.getName();
        BillingPlan plan = customer.getPlan();
        int weeksDelinquent = customer.getHistory().getWeeksDelinquent();
        return "name:" + customerName + ",plan:" + plan + ",weeksDelinquent:" + weeksDelinquent;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        if (customer == null) {
            return new NullCustomer();
        }
        return customer;
    }
}
