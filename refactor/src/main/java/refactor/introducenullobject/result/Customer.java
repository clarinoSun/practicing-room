package refactor.introducenullobject.result;

public class Customer {
    private String name;
    private BillingPlan plan;
    private PaymentHistory history;

    public Customer() {
    }

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public BillingPlan getPlan() {
        return plan;
    }

    public void setPlan(BillingPlan plan) {
        this.plan = plan;
    }

    public PaymentHistory getHistory() {
        return history;
    }

    public void setHistory(PaymentHistory history) {
        this.history = history;
    }
}
