package refactor.introducenullobject.result;

public class NullPaymentHistory extends PaymentHistory {
    @Override
    public int getWeeksDelinquent() {
        return 0;
    }
}
