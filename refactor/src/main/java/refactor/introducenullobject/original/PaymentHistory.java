package refactor.introducenullobject.original;

public class PaymentHistory {
    private int weeksDelinquent;

    public PaymentHistory(int weeksDelinquent) {
        this.weeksDelinquent = weeksDelinquent;
    }

    public int getWeeksDelinquent() {
        return weeksDelinquent;
    }

}
