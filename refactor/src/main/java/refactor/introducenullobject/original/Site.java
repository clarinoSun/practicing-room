package refactor.introducenullobject.original;

public class Site {
    private Customer customer;

    public String state() {
        Customer customer = getCustomer();
        BillingPlan plan;
        if (customer == null) {
            plan = BillingPlan.getBasic();
        } else {
            plan = customer.getPlan();
        }
        String customerName;
        if (customer == null) {
            customerName = "occupant";
        } else {
            customerName = customer.getName();
        }
        int weeksDelinquent;
        if (customer == null) {
            weeksDelinquent = 0;
        } else {
            weeksDelinquent = customer.getHistory().getWeeksDelinquent();
        }
        return "name:" + customerName + ",plan:" + plan + ",weeksDelinquent:" + weeksDelinquent;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }
}
