package refactor.introducenullobject.original;

public class BillingPlan {
    private String name;

    public BillingPlan(String name) {
        this.name = name;
    }

    public static BillingPlan getBasic() {
        return new BillingPlan("basic");
    }

    @Override
    public String toString() {
        return name;
    }
}
