package refactor.replaceconditionalwithpolymorphism.result;

public class Manager extends EmployeeType {
    @Override
    public int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary() + employee.getBonus();
    }
}
