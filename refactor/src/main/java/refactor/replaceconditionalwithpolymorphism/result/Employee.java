package refactor.replaceconditionalwithpolymorphism.result;

public class Employee {
    private EmployeeType type;
    private int monthlySalary;
    private int commission;
    private int bonus;

    public Employee(EmployeeType type, int monthlySalary, int commission, int bonus) {
        this.type = type;
        this.monthlySalary = monthlySalary;
        this.commission = commission;
        this.bonus = bonus;
    }

    public int calcPayAmount() {
        return type.calcPayAmount(this);
    }

    public int getMonthlySalary() {
        return monthlySalary;
    }

    public int getCommission() {
        return commission;
    }

    public int getBonus() {
        return bonus;
    }
}
