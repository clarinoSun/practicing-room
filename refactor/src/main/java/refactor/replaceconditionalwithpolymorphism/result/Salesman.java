package refactor.replaceconditionalwithpolymorphism.result;

public class Salesman extends EmployeeType {
    @Override
    public int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary() + employee.getCommission();
    }
}
