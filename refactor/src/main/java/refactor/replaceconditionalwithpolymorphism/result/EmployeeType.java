package refactor.replaceconditionalwithpolymorphism.result;

public abstract class EmployeeType {
    public abstract int calcPayAmount(Employee employee);
}
