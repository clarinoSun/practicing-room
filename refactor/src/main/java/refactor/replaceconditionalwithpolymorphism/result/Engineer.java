package refactor.replaceconditionalwithpolymorphism.result;

public class Engineer extends EmployeeType {
    @Override
    public int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary();
    }
}
