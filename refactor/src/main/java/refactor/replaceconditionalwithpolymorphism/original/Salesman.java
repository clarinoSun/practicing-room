package refactor.replaceconditionalwithpolymorphism.original;

public class Salesman extends EmployeeType {
    @Override
    public int getTypeCode() {
        return 2;
    }
}
