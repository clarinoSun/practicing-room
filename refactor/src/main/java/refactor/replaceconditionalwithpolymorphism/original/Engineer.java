package refactor.replaceconditionalwithpolymorphism.original;

public class Engineer extends EmployeeType {
    @Override
    public int getTypeCode() {
        return 1;
    }
}
