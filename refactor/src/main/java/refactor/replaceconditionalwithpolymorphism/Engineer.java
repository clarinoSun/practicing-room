package refactor.replaceconditionalwithpolymorphism;

public class Engineer extends EmployeeType {

    @Override
    int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary();
    }
}
