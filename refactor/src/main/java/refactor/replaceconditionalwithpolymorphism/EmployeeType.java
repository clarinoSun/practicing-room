package refactor.replaceconditionalwithpolymorphism;

public abstract class EmployeeType {
    abstract int calcPayAmount(Employee employee);
}
