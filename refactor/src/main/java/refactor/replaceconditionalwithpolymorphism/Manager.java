package refactor.replaceconditionalwithpolymorphism;

public class Manager extends EmployeeType {

    @Override
    int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary() + employee.getBonus();
    }
}
