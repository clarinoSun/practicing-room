package refactor.replaceconditionalwithpolymorphism;

public class Salesman extends EmployeeType {

    @Override
    int calcPayAmount(Employee employee) {
        return employee.getMonthlySalary() + employee.getCommission();
    }
}
