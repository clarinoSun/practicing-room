package refactor.replaceexceptionwithtest;

import java.util.Stack;

public class ResourcePool {
    private Stack<Resource> available;
    private Stack<Resource> allocated;

    public Resource getResource() {
        Resource result;
        if (available.isEmpty()) {
            result = new Resource();
        } else {
            result = available.pop();
        }
        allocated.push(result);
        return result;
    }
}
