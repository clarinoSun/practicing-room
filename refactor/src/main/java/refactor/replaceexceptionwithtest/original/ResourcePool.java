package refactor.replaceexceptionwithtest.original;

import java.util.EmptyStackException;
import java.util.Stack;

public class ResourcePool {
    private Stack<Resource> available;
    private Stack<Resource> allocated;

    public Resource getResource() {
        Resource result;
        try {
            result = available.pop();
            allocated.push(result);
            return result;
        } catch (EmptyStackException e) {
            result = new Resource();
            allocated.push(result);
            return result;
        }
    }
}
