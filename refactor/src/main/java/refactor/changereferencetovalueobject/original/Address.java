package refactor.changereferencetovalueobject.original;

import org.assertj.core.util.Lists;

import java.util.List;

public class Address {
    private String id;
    private String address;
    private static List<Address> instances;

    private Address(String id, String address) {
        this.id = id;
        this.address = address;
    }

    private static void loadInstances() {
        instances = Lists.newArrayList(new Address("1", "company 17 floor"), new Address("2", "home"));
    }

    public static Address get(String address) {
        if (instances == null) {
            loadInstances();
        }
        return instances.stream().filter(instance -> address.equals(instance.getAddress())).findFirst().orElse(null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
