package refactor.changereferencetovalueobject;

public class Order {
    private String id;
    private Address address;

    public Order(String id) {
        this.id = id;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String state() {
        return "order id:" + id + ",address:" + address.getAddress();
    }
}
