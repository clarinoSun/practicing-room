package refactor.replacenestedconditionalwithguardclauses;

public class ReplaceNestedConditionalWithGuardClausesExample {
    public double calcPayAmount(boolean isDead, boolean isSeperated, boolean isRetired, double basicAmount) {
        if (basicAmount <= 0) {
            return 0;
        }
        if (isDead) {
            return calcDeadAmount();
        }
        if (isSeperated) {
            return calcSeperatedAmount();
        }
        if (isRetired) {
            return calcRetiredAmount();
        }
        return calcNormalAmount();
    }

    private double calcNormalAmount() {
        return 0;
    }

    private double calcRetiredAmount() {
        return 0;
    }

    private double calcSeperatedAmount() {
        return 0;
    }

    private double calcDeadAmount() {
        return 0;
    }
}
