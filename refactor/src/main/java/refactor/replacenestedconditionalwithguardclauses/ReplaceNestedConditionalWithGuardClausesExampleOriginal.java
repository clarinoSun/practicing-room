package refactor.replacenestedconditionalwithguardclauses;

public class ReplaceNestedConditionalWithGuardClausesExampleOriginal {
    public double calcPayAmount(boolean isDead, boolean isSeperated, boolean isRetired, double basicAmount) {
        double result = 0;
        if (basicAmount > 0) {
            if (isDead) {
                result = calcDeadAmount();
            } else {
                if (isSeperated) {
                    result = calcSeperatedAmount();
                } else {
                    if (isRetired) {
                        result = calcRetiredAmount();
                    } else {
                        result = calcNormalAmount();
                    }
                }
            }
        }
        return result;
    }

    private double calcNormalAmount() {
        return 0;
    }

    private double calcRetiredAmount() {
        return 0;
    }

    private double calcSeperatedAmount() {
        return 0;
    }

    private double calcDeadAmount() {
        return 0;
    }
}
