package refactor.parameterizemethod;

public class Employee {
    private double salary;

    public void tenPercentRaise() {
        salary *= 1.1;
    }

    public void fivePercentRaise() {
        salary *= 1.05;
    }

    public void raise(double factor) {
        salary *= (1 + factor);
    }

    public Employee(double salary) {
        this.salary = salary;
    }

    public double calcBaseCharge() {
        double result = calcSalaryInRange(0, 10000) * 0.03;
        result += calcSalaryInRange(10000, 20000) * 0.05;
        result += calcSalaryInRange(20000, Integer.MAX_VALUE) * 0.07;
        return result;
    }

    public double calcSalaryInRange(int start, int end) {
        if (salary > start) {
            return Math.min(salary, end) - start;
        }
        return 0;
    }
}
