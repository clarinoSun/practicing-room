package refactor.parameterizemethod;

public class EmployeeOriginal {
    private double salary;

    public void tenPercentRaise() {
        salary *= 1.1;
    }

    public void fivePercentRaise() {
        salary *= 1.05;
    }

    public EmployeeOriginal(double salary) {
        this.salary = salary;
    }

    public double calcBaseCharge() {
        double result = Math.min(salary, 10000) * 0.03;
        if (salary > 10000) {
            result += (Math.min(salary, 20000) - 10000) * 0.05;
        }
        if (salary > 20000) {
            result += (salary - 20000) * 0.07;
        }
        return result;
    }


}
