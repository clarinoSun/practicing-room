package refactor.replacedatavaluewithobject;

public class Student {
    private String name;
    private String telephone;
    private String telephoneAreaCode;

    public Student(String name) {
        this.name = name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
        this.telephoneAreaCode = telephone.substring(0, 3);
    }

    public boolean isTelephoneCorrect() {
        return "010".equals(telephoneAreaCode);
    }
}
