package refactor.replacedatavaluewithobject;

public class Telephone {
    String telephone;
    String telephoneAreaCode;

    public Telephone() {
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
        this.telephoneAreaCode = telephone.substring(0, 3);
    }

    public boolean isTelephoneCorrect() {
        return "010".equals(telephoneAreaCode);
    }
}