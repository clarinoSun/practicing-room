package refactor.replacedatavaluewithobject.result;

public class Telephone {
    String number;
    String areaCode;

    public Telephone() {
    }

    public void setNumber(String number) {
        this.number = number;
        this.areaCode = number.substring(0, 3);
    }

    public boolean isTelephoneCorrect() {
        return "010".equals(areaCode);
    }
}