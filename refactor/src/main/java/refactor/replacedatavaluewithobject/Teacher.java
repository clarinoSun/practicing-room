package refactor.replacedatavaluewithobject;

public class Teacher {
    private final Telephone telephone = new Telephone();
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    public void setTelephone(String telephoneNumber) {
        telephone.setTelephone(telephoneNumber);
    }

    public boolean isTelephoneCorrect() {
        return telephone.isTelephoneCorrect();
    }
}
