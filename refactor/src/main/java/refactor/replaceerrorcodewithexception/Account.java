package refactor.replaceerrorcodewithexception;

public class Account {
    private double balance;

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void withdraw(double amount) throws BalanceNotEnoughException {
        if (amount > balance) {
            throw new BalanceNotEnoughException();
        }
        balance -= amount;
    }

    public void clientMethod() {
        try {
            withdraw(500);
            doTheUsualThing();
        } catch (BalanceNotEnoughException e) {
            handleOverdrawn();
        }
    }

    public void clientMethod2() {
        try {
            withdraw(500);
            doTheUsualThing();
        } catch (BalanceNotEnoughException e) {
            handleOverdrawn();
        }
    }

    private void doTheUsualThing() {

    }

    private void handleOverdrawn() {

    }
}
