package refactor.replaceerrorcodewithexception.original;

public class Account {
    private double balance;

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int withdraw(double amount) {
        if (amount > balance) {
            return -1;
        } else {
            balance -= amount;
            return 0;
        }
    }

    public void clientMethod() {
        if (withdraw(500) == -1) {
            handleOverdrawn();
        } else {
            doTheUsualThing();
        }
    }

    private void doTheUsualThing() {

    }

    private void handleOverdrawn() {

    }
}
