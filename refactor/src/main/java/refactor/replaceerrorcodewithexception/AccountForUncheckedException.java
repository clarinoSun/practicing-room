package refactor.replaceerrorcodewithexception;

public class AccountForUncheckedException {
    private double balance;

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void withdraw(double amount) {
        if (amount > balance) {
            throw new IllegalArgumentException("Amount too large");
        }
        balance -= amount;
    }

    public void clientMethod() {
        if (!clientCanWithdraw(500)) {
            handleOverdrawn();
        }
        withdraw(500);
        doTheUsualThing();
    }

    public boolean clientCanWithdraw(double amount) {
        return true;
    }

    private void doTheUsualThing() {

    }

    private void handleOverdrawn() {

    }
}
