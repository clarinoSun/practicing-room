package refactor.splittemporaryvariable;

public class SplitTemporaryVariableExampleOriginal {
    private double primaryForce;
    private double mass;
    private int initPrimaryTime;
    private double secondaryForce;

    public SplitTemporaryVariableExampleOriginal(double primaryForce, double mass, int initPrimaryTime, double secondaryForce) {
        this.primaryForce = primaryForce;
        this.mass = mass;
        this.initPrimaryTime = initPrimaryTime;
        this.secondaryForce = secondaryForce;
    }

    double calcDistanceTravelled(int time) {
        double result;
        double acc = primaryForce / mass;
        int primaryTime = Math.min(time, initPrimaryTime);
        result = 0.5 * acc * primaryTime * primaryTime;
        int secondaryTime = time - initPrimaryTime;
        if (secondaryTime > 0) {
            double primaryVel = acc * initPrimaryTime;
            acc = (primaryForce + secondaryForce) / mass;
            result += primaryVel * secondaryTime + 0.5 * acc * secondaryTime * secondaryTime;
        }
        return result;
    }

}
