package refactor.splittemporaryvariable;

public class SplitTemporaryVariableExample {
    private double primaryForce;
    private double mass;
    private int initPrimaryTime;
    private double secondaryForce;

    public SplitTemporaryVariableExample(double primaryForce, double mass, int initPrimaryTime, double secondaryForce) {
        this.primaryForce = primaryForce;
        this.mass = mass;
        this.initPrimaryTime = initPrimaryTime;
        this.secondaryForce = secondaryForce;
    }

    double calcDistanceTravelled(int time) {
        double result;
        double primaryAcc = primaryForce / mass;
        int primaryTime = Math.min(time, initPrimaryTime);
        result = 0.5 * primaryAcc * primaryTime * primaryTime;
        int secondaryTime = time - initPrimaryTime;
        if (secondaryTime > 0) {
            double primaryVel = primaryAcc * initPrimaryTime;
            double secondaryAcc = (primaryForce + secondaryForce) / mass;
            result += primaryVel * secondaryTime + 0.5 * secondaryAcc * secondaryTime * secondaryTime;
        }
        return result;
    }

}