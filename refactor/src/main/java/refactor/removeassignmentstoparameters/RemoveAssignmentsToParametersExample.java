package refactor.removeassignmentstoparameters;

public class RemoveAssignmentsToParametersExample {

    public int changeSimpleTypeParameterValueAndReturn(int inputVal) {
        int result = inputVal;
        if (inputVal < 50) {
            result = inputVal * 2;
        }
        if (inputVal > 60) {
            result = inputVal + 1;
        }
        return result;
    }

    public void changeObjectParameterProperties(Book book) {
        book.setName("重构");
    }

    public void changeSimpleTypeParameterValue(int inputVal) {
        inputVal = inputVal * 2;
        System.out.println("在我的方法体里以后就让inputVal变了意思，变成" + inputVal);
    }

    public void changeObjectParameterReference(Book book) {
        final String originalName = book.getName();
        book = new Book("java编程思想");
        System.out.println("在我的方法体里从这以后就让" + originalName + "这本书代表：" + book.getName());
        book = new Book("DDD领域驱动设计");
        System.out.println("在我的方法体里从这以后就让" + originalName + "这本书代表：" + book.getName());
    }

}