package refactor.refactorfourstpes.original;

import refactor.refactorfourstpes.Rental;

import java.util.List;
import java.util.Vector;

public class Customer {
    private List<refactor.refactorfourstpes.Rental> rentals;
    private String name;

    public Customer(Vector rentals, String name) {
        this.rentals = rentals;
        this.name = name;
    }

    public String state() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        String result = "Rental Record for " + getName() + "\n";
        for (Rental each : rentals) {
            double thisAmount = 0;
            //determine amounts for each line
            switch (each.getMovie().getPriceCode()) {
                case refactor.refactorfourstpes.Movie.REGULAR:
                    thisAmount += 2;
                    if (each.getDaysRented() > 2) {
                        thisAmount += (each.getDaysRented() - 2) * 1.5;
                    }
                    break;
                case refactor.refactorfourstpes.Movie.NEW_RELEASE:
                    thisAmount += each.getDaysRented() * 3;
                    break;
                case refactor.refactorfourstpes.Movie.CHILDREN:
                    thisAmount += 1.5;
                    if (each.getDaysRented() > 3) {
                        thisAmount += (each.getDaysRented() - 3) * 1.5;
                    }
                    break;
            }
            // add frequent renter points
            frequentRenterPoints++;
            // add bonus for a two day new release rental
            if ((each.getMovie().getPriceCode() == refactor.refactorfourstpes.original.Movie.NEW_RELEASE) &&
                    each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }
            //show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" +
                    String.valueOf(thisAmount) + "\n";
            totalAmount += thisAmount;
        }
        //add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints) +
                " frequent renter points";
        return result;
    }

    public String getName() {
        return name;
    }

}