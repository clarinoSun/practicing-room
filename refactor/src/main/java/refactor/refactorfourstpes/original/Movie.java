package refactor.refactorfourstpes.original;

public class Movie {
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;
    public static final int CHILDREN = 2;
    private int priceCode;
    private String title;

    public Movie(int priceCode, String title) {
        this.priceCode = priceCode;
        this.title = title;
    }

    public int getPriceCode() {
        return priceCode;
    }

    public String getTitle() {
        return title;
    }

}