package refactor.extractclass.original;

public class Person {
    private String name;
    private String officeAreaCode;
    private String officeNumber;

    public Person(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return "(" + officeAreaCode + ")" + officeNumber;
    }

    public void setOfficeAreaCode(String officeAreaCode) {
        this.officeAreaCode = officeAreaCode;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }
}