package refactor.extractclass;

public class TelephoneNumber {
    private String areaCode;
    private String number;

    public TelephoneNumber() {
    }

    public String getTelephoneNumber() {
        return "(" + areaCode + ")" + number;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}