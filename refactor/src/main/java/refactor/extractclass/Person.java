package refactor.extractclass;

public class Person {
    private final TelephoneNumber officeTelephone = new TelephoneNumber();
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return getOfficeTelephone().getTelephoneNumber();
    }

    public TelephoneNumber getOfficeTelephone() {
        return officeTelephone;
    }
}