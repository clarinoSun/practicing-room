package refactor.removemiddleman;

public class Department {
    private refactor.removemiddleman.Person manager;

    public Department(refactor.removemiddleman.Person manager) {
        this.manager = manager;
    }

    public refactor.removemiddleman.Person getManager() {
        return manager;
    }
}