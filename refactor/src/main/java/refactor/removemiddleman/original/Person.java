package refactor.removemiddleman.original;

public class Person {
    private String name;
    private Department department;

    public Person(String name) {
        this.name = name;
    }

    public Person getManager() {
        return department.getManager();
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }
}
