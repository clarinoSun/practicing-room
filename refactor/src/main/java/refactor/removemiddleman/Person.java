package refactor.removemiddleman;

public class Person {
    private String name;
    private Department department;

    public Person(String name) {
        this.name = name;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }
}
