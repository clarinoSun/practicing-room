package refactor.replacemethodwithmethodobject;

public class PriceCalculator {
    private double itemPrice;
    private int quantity;
    private int calcFactor;
    private double basePrice;
    private double secondBasePrice;
    private double thirdBasePrice;
    private double fourthBasePrice;
    private final ReplaceMethodWithMethodObjectExample replaceMethodWithMethodObjectExample;

    public PriceCalculator(double itemPrice, int quantity, int calcFactor,
                           ReplaceMethodWithMethodObjectExample replaceMethodWithMethodObjectExample) {
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.calcFactor = calcFactor;
        this.replaceMethodWithMethodObjectExample = replaceMethodWithMethodObjectExample;
    }

    public double calc() {
        basePrice = itemPrice * quantity;
        secondBasePrice = itemPrice * quantity * calcFactor;
        thirdBasePrice = itemPrice * quantity * calcFactor * calcFactor;
        fourthBasePrice = 0;
        double result = 0;
        if (thirdBasePrice - secondBasePrice > 200) {
            fourthBasePrice = thirdBasePrice * 2;
            replaceMethodWithMethodObjectExample.callMethod();
        }
        if (secondBasePrice - basePrice > 100) {
            result += basePrice - 20;
        }
        result += basePrice + secondBasePrice + thirdBasePrice + fourthBasePrice;
        return result;
    }

}