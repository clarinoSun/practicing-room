package refactor.replacemethodwithmethodobject.result;

public class PriceCalculator {
    private double basePrice;
    private double secondBasePrice;
    private double thirdBasePrice;
    private double fourthBasePrice;
    private final refactor.replacemethodwithmethodobject.result.ReplaceMethodWithMethodObjectExampleResult replaceMethodWithMethodObjectExampleResult;
    private double itemPrice;
    private int quantity;
    private int calcFactor;

    public PriceCalculator(refactor.replacemethodwithmethodobject.result.ReplaceMethodWithMethodObjectExampleResult replaceMethodWithMethodObjectExampleResult,
                           double itemPrice, int quantity, int calcFactor) {
        this.replaceMethodWithMethodObjectExampleResult = replaceMethodWithMethodObjectExampleResult;
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.calcFactor = calcFactor;
    }

    public double calc() {
        basePrice = itemPrice * quantity;
        secondBasePrice = itemPrice * quantity * calcFactor;
        thirdBasePrice = itemPrice * quantity * calcFactor * calcFactor;
        fourthBasePrice = 0;
        double result = 0;
        if (thirdBasePrice - secondBasePrice > 200) {
            fourthBasePrice = thirdBasePrice * 2;
            replaceMethodWithMethodObjectExampleResult.callMethod();
        }
        if (secondBasePrice - basePrice > 100) {
            result += basePrice - 20;
        }
        result += basePrice + secondBasePrice + thirdBasePrice + fourthBasePrice;
        return result;
    }

}