package refactor.replacemethodwithmethodobject.result;

public class ReplaceMethodWithMethodObjectExampleResult {

    public double calcPrice(double itemPrice, int quantity, int calcFactor) {
        return new PriceCalculator(this, itemPrice, quantity, calcFactor).calc();
    }

    public void callMethod() {

    }
}