package refactor.replacemethodwithmethodobject;

public class ReplaceMethodWithMethodObjectExample {

    public double calcPrice(double itemPrice, int quantity, int calcFactor) {
        return new PriceCalculator(itemPrice, quantity, calcFactor, this).calc();
    }

    public void callMethod() {

    }
}