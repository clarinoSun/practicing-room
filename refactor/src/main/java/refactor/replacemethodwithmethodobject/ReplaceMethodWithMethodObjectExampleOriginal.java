package refactor.replacemethodwithmethodobject;

public class ReplaceMethodWithMethodObjectExampleOriginal {

    public double calcPrice(double itemPrice, int quantity, int calcFactor) {
        double basePrice = itemPrice * quantity;
        double secondBasePrice = itemPrice * quantity * calcFactor;
        double thirdBasePrice = itemPrice * quantity * calcFactor * calcFactor;
        double fourthBasePrice = 0;
        double result = 0;
        if (thirdBasePrice - secondBasePrice > 200) {
            fourthBasePrice = thirdBasePrice * 2;
            callMethod();
        }
        if (secondBasePrice - basePrice > 100) {
            result += basePrice - 20;
        }
        result += basePrice + secondBasePrice + thirdBasePrice + fourthBasePrice;
        return result;
    }

    private void callMethod() {

    }
}