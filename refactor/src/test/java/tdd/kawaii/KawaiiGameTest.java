package tdd.kawaii;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class KawaiiGameTest {
    @Test
    public void should_return_ka_when_calc_given_number_between_1_and_200_and_contains_3() {
        // given
        int input = 13;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("ka");
    }

    @Test
    public void should_return_ka_when_calc_given_number_between_1_and_200_and_multiple_of_3_and_not_contains_3() {
        // given
        int input = 6;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("ka");
    }


    @Test
    public void should_return_wa_when_calc_given_number_between_1_and_200_and_multiple_of_5_and_not_contains_3() {
        // given
        int input = 10;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("wa");
    }

    @Test
    public void should_return_ii_when_calc_given_number_between_1_and_200_and_multiple_of_7_and_not_contains_3() {
        // given
        int input = 14;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("ii");
    }


    @Test
    public void should_return_kawa_when_calc_given_number_between_1_and_200_and_multiple_of_3_and_5_and_not_contains_3() {
        // given
        int input = 15;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("kawa");
    }

    @Test
    public void should_return_kaii_when_calc_given_number_between_1_and_200_and_multiple_of_3_and_7_and_not_contains_3() {
        // given
        int input = 21;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("kaii");
    }


    @Test
    public void should_return_waii_when_calc_given_number_between_1_and_200_and_multiple_of_5_and_7_and_not_contains_3() {
        // given
        int input = 70;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("waii");
    }

    @Test
    public void should_return_kawaii_when_calc_given_number_between_1_and_200_and_multiple_of_3_and_5_and_7_and_not_contains_3() {
        // given
        int input = 105;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("kawaii");
    }

    @Test
    public void should_return_itself_when_calc_given_number_between_1_and_200_and_not_multiple_of_3_or_5_or_7_and_not_contains_3() {
        // given
        int input = 4;
        // when
        final String result = new KawaiiGame().calc(input);
        // then
        assertThat(result).isEqualTo("4");
    }


    @Test
    public void should_throw_exception_when_calc_given_number_between_smaller_than_1() {
        // given
        int input = 0;
        // when
        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> new KawaiiGame().calc(input));
    }

    @Test
    public void should_throw_exception_when_calc_given_number_between_bigger_than_200() {
        // given
        int input = 201;
        // when
        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> new KawaiiGame().calc(input));
    }
}