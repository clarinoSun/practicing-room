package techinalframework.mapstruct.easiest;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CarMapperTest {
    @Test
    void should_map_car_to_dto_when_carToCarDto_given_car() {
        // given
        Car car = new Car("PORSCHE", 5, CarType.AUTOMATIC);
        // when
        CarDTO carDTO = CarMapper.INSTANCE.carToCarDTO(car);
        // then
        assertThat(carDTO).isNotNull();
        assertThat(carDTO.getBrand()).isEqualTo("PORSCHE");
        assertThat(carDTO.getSeatCount()).isEqualTo(5);
        assertThat(carDTO.getType()).isEqualTo("AUTOMATIC");
    }
}