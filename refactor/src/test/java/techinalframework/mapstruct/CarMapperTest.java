package techinalframework.mapstruct;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CarMapperTest {
    @Test
    public void should_map_car_to_dto_when_carToCarDto_given_car() {
        // given
        Car car = new Car("Morris", 5, CarType.PORSCHE);
        // when
        CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);
        // then
        assertThat(carDto).isNotNull();
        assertThat(carDto.getMake()).isEqualTo("Morris");
        assertThat(carDto.getSeatCount()).isEqualTo(5);
        assertThat(carDto.getType()).isEqualTo("PORSCHE");
    }
}