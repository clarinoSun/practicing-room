package techinalframework.mapstruct.complicated;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CarMapperTest {

    @Test
    void should_map_car_to_dto_when_carToCarDto_given_car() {
        // given
        Car car = new Car("Morris", 5, CarType.PORSCHE);
        car.setWheel(new Wheel("spare tire"));
        car.setIgnoredProperty("ignore");
        List<Integer> feeAmounts = Lists.newArrayList(100);
        car.setFeeAmounts(feeAmounts);
        car.setCustomField("customField");
        // when
        CarDTO carDTO = CarMapper.INSTANCE.carToCarDTO(car);
        // then
        assertThat(carDTO).isNotNull();
        assertThat(carDTO.getMake()).isEqualTo("Morris");
        assertThat(carDTO.getSeatCount()).isEqualTo(5);
        assertThat(carDTO.getType()).isEqualTo("PORSCHE");
        assertThat(carDTO.getWheelDTO().getTypeName()).isEqualTo("spare tire");
        assertThat(car.getIgnoredProperty()).isEqualTo("ignore");
        assertThat(carDTO.getIgnoredProperty()).isEqualTo(null);
        assertThat(carDTO.getFeeAmounts().get(0)).isEqualTo("100.00");
        assertThat(carDTO.getCustomizedField()).isEqualTo("customField customized method");
    }

    @Test
    void should_map_car_dto_to_car_when_carDTOToCar_given_carDTO() {
        // given
        CarDTO carDTO = new CarDTO("Morris", 5, CarType.PORSCHE.name());
        carDTO.setWheelDTO(new WheelDTO("spare tire"));
        carDTO.setIgnoredProperty("ignore");
        List<String> feeAmounts = Lists.newArrayList("100");
        carDTO.setFeeAmounts(feeAmounts);
        carDTO.setCustomizedField("customField");
        // when
        Car car = CarMapper.INSTANCE.carDTOToCar(carDTO);

        // then
        assertThat(car).isNotNull();
        assertThat(car.getMake()).isEqualTo("Morris");
        assertThat(car.getNumberOfSeats()).isEqualTo(5);
        assertThat(car.getType().name()).isEqualTo("PORSCHE");
        assertThat(car.getWheel().getType()).isEqualTo("spare tire");
        assertThat(carDTO.getIgnoredProperty()).isEqualTo("ignore");
        assertThat(car.getIgnoredProperty()).isEqualTo(null);
        assertThat(car.getFeeAmounts().get(0)).isEqualTo(100);
        assertThat(car.getCustomField()).isEqualTo("customField customized method back");
    }
}