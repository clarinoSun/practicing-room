package refactor.removemiddleman;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {
    @Test
    public void should_return_manager_when_getManagerFromPerson_given_person_with_department() {
        // given
        final refactor.removemiddleman.Person sam = new refactor.removemiddleman.Person("sam");
        sam.setDepartment(new refactor.removemiddleman.Department(new refactor.removemiddleman.Person("bob")));
        // when
        final refactor.removemiddleman.Person manager = sam.getDepartment().getManager();
        // then
        assertThat(manager.getName()).isEqualTo("bob");
    }
}