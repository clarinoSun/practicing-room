package refactor.removecontrolflag;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RemoveControlFlagExampleTest {

    @Test
    public void should_sendAlert_when_checkSecurity_given_people_contains_sam_or_tom() {
        // given
        RemoveControlFlagExample removeControlFlagExample = new RemoveControlFlagExample();
        // when
        boolean checkSecurity = removeControlFlagExample.checkSecurity(new String[]{"sam", "nick"});
        // then
        assertThat(checkSecurity).isTrue();
    }

    @Test
    public void should_sendAlert_when_checkSecurity_given_people_contains_tom() {
        // given
        RemoveControlFlagExample removeControlFlagExample = new RemoveControlFlagExample();
        // when
        boolean checkSecurity = removeControlFlagExample.checkSecurity(new String[]{"tom", "nick"});
        // then
        assertThat(checkSecurity).isTrue();
    }


    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_sam() {
        // given
        RemoveControlFlagExample removeControlFlagExample = new RemoveControlFlagExample();
        // when
        String miscreant = removeControlFlagExample.findMiscreant(new String[]{"sam", "nick"});
        // then
        assertThat(miscreant).isEqualTo("sam");
    }

    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_tom() {
        // given
        RemoveControlFlagExample removeControlFlagExample = new RemoveControlFlagExample();
        // when
        String miscreant = removeControlFlagExample.findMiscreant(new String[]{"tom", "nick"});
        // then
        assertThat(miscreant).isEqualTo("tom");
    }
}