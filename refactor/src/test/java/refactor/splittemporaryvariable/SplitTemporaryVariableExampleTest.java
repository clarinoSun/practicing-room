package refactor.splittemporaryvariable;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SplitTemporaryVariableExampleTest {
    @Test
    public void should_return_distance_with_secondary_force_when_calcDistanceTravelled_given_time_more_than_initPrimaryTime() {
        // given
        final SplitTemporaryVariableExample splitTemporaryVariableExample = new SplitTemporaryVariableExample(1000, 100, 5, 500);
        // when
        final double result = splitTemporaryVariableExample.calcDistanceTravelled(10);
        // then
        assertThat(result).isEqualTo(562.5);
    }
}