package refactor.introducenullobject;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SiteTest {
    @Test
    void should_return_site_info_when_state_given_not_null_customer() {
        // given
        Site site = new Site();
        Customer tom = new Customer("tom");
        tom.setPlan(new BillingPlan("plan b"));
        tom.setHistory(new PaymentHistory(6));
        site.setCustomer(tom);
        // when
        String state = site.state();
        // then
        assertThat(state).isEqualTo("name:tom,plan:plan b,weeksDelinquent:6");
    }

    @Test
    void should_return_site_info_when_state_given_null_customer() {
        // given
        Site site = new Site();
        site.setCustomer(null);
        // when
        String state = site.state();
        // then
        assertThat(state).isEqualTo("name:occupant,plan:basic,weeksDelinquent:0");
    }
}