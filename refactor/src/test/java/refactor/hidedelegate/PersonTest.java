package refactor.hidedelegate;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {
    @Test
    public void should_return_manager_when_getManagerFromPerson_given_person_with_department() {
        // given
        final Person sam = new Person("sam");
        sam.setDepartment(new Department(new Person("bob")));
        // when
        final Person manager = sam.getManager();
        // then
        assertThat(manager.getName()).isEqualTo("bob");
    }
}