package refactor.removeassignmentstoparameters;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RemoveAssignmentsToParametersExampleTest {

    @Test
    public void should_object_parameter_properties_be_changed_when_changeObjectParameterProperties_given_book_parameter() {
        // given
        final RemoveAssignmentsToParametersExample removeAssignmentsToParametersExample = new RemoveAssignmentsToParametersExample();
        Book book = new Book("重购");
        // when
        removeAssignmentsToParametersExample.changeObjectParameterProperties(book);
        // then
        assertThat(book.getName()).isEqualTo("重构");
    }

    @Test
    public void should_object_parameter_reference_remain_the_same_when_changeObjectParameterReference_given_book_parameter() {
        // given
        final RemoveAssignmentsToParametersExample removeAssignmentsToParametersExample = new RemoveAssignmentsToParametersExample();
        Book book = new Book("重构");
        final String initBookReference = book.toString();
        // when
        removeAssignmentsToParametersExample.changeObjectParameterReference(book);
        // then
        assertThat(book.getName()).isEqualTo("重构");
        assertThat(book.toString()).isEqualTo(initBookReference);
    }


    @Test
    public void should_simple_type_parameter_value_be_changed_when_changeSimpleTypeParameterValueAndReturn_given_simple_type_parameter() {
        // given
        final RemoveAssignmentsToParametersExample removeAssignmentsToParametersExample = new RemoveAssignmentsToParametersExample();
        int inputVal = 2;
        // when
        final int result = removeAssignmentsToParametersExample.changeSimpleTypeParameterValueAndReturn(inputVal);
        // then
        assertThat(result).isEqualTo(4);
    }


    @Test
    public void should_simple_type_parameter_value_not_be_changed_when_changeSimpleTypeParameterValue_given_simple_type_parameter() {
        // given
        final RemoveAssignmentsToParametersExample removeAssignmentsToParametersExample = new RemoveAssignmentsToParametersExample();
        int inputVal = 2;
        // when
        removeAssignmentsToParametersExample.changeSimpleTypeParameterValue(inputVal);
        // then
        assertThat(inputVal).isEqualTo(2);
    }
}