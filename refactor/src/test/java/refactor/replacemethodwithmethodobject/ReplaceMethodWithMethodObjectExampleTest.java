package refactor.replacemethodwithmethodobject;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ReplaceMethodWithMethodObjectExampleTest {

    final ReplaceMethodWithMethodObjectExample replaceMethodWithMethodObjectExample =
            new ReplaceMethodWithMethodObjectExample();

    @Test
    public void should_return_price_when_calcPrice_given_itemPrice_quantity_calcFactor() {
        // given
        final int itemPrice = 200;
        final int quantity = 100;
        final int calcFactor = 5;
        // when
        final double result = replaceMethodWithMethodObjectExample.calcPrice(itemPrice, quantity, calcFactor);
        // then
        assertThat(result).isEqualTo(1639980.0);
    }


    @Test
    public void should_price_be_the_same_after_refactored_when_calcPriceRefactor_given_same_itemPrice_quantity_calcFactor() {
        // given
        final int itemPrice = 200;
        final int quantity = 100;
        final int calcFactor = 5;
        final double priceBeforeRefactored = replaceMethodWithMethodObjectExample.calcPrice(itemPrice, quantity, calcFactor);
        // when
        final double result = new PriceCalculator(itemPrice, quantity, calcFactor, replaceMethodWithMethodObjectExample).calc();
        // then
        assertThat(result).isEqualTo(priceBeforeRefactored);
    }
}