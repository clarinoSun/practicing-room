package refactor.changevaluetoreference;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ClientTest {
    private Client client;

    @BeforeEach
    void setUp() {
        client = new Client();
    }

    @Test
    public void should_return_total_count_when_countOrdersForCustomer_given_orders_with_same_customer_name() {
        // given
        List<Order> orders = Lists.newArrayList(new Order("Tom"), new Order("Tom"), new Order("Sam"));
        String customer = "Tom";
        // when
        long count = client.countOrdersForCustomer(orders, customer);
        // then
        assertThat(count).isEqualTo(2);
    }


    @Test
    public void should_return_total_count_when_countOrdersForCustomer_given_orders_with_same_customer_reference() {
        // given
        List<Order> orders = Lists.newArrayList(new Order("Tom"), new Order("Tom"), new Order("Sam"));
        Customer customer = Customer.getNamed("Tom");
        // when
        long count = client.countOrdersForCustomer(orders, customer);
        // then
        assertThat(count).isEqualTo(2);
    }
}