package refactor.separatequeryfrommodifier;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SeparateQueryFromModifierExampleOriginalTest {

    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_sam() {
        // given
        SeparateQueryFromModifierExampleOriginal separateQueryFromModifierExampleOriginal = new SeparateQueryFromModifierExampleOriginal();
        // when
        String miscreant = separateQueryFromModifierExampleOriginal.findMiscreant(new String[]{"sam", "nick"});
        // then
        assertThat(miscreant).isEqualTo("sam");
    }

    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_tom() {
        // given
        SeparateQueryFromModifierExampleOriginal separateQueryFromModifierExampleOriginal = new SeparateQueryFromModifierExampleOriginal();
        // when
        String miscreant = separateQueryFromModifierExampleOriginal.findMiscreant(new String[]{"tom", "nick"});
        // then
        assertThat(miscreant).isEqualTo("tom");
    }

    @Test
    public void should_return_empty_when_findMiscreant_given_people_not_contains_tom_and_sam() {
        // given
        SeparateQueryFromModifierExampleOriginal separateQueryFromModifierExampleOriginal = new SeparateQueryFromModifierExampleOriginal();
        // when
        String miscreant = separateQueryFromModifierExampleOriginal.findMiscreant(new String[]{"tim", "nick"});
        // then
        assertThat(miscreant).isEqualTo("");
    }
}