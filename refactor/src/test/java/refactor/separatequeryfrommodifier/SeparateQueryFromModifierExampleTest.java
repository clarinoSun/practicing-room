package refactor.separatequeryfrommodifier;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SeparateQueryFromModifierExampleTest {

    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_sam() {
        // given
        SeparateQueryFromModifierExample separateQueryFromModifierExample = new SeparateQueryFromModifierExample();
        // when
        String[] people = {"sam", "nick"};
        separateQueryFromModifierExample.findMiscreant(people);
        String miscreant = separateQueryFromModifierExample.findPeople(people);
        // then
        assertThat(miscreant).isEqualTo("sam");
    }

    @Test
    public void should_return_miscreant_when_findMiscreant_given_people_contains_tom() {
        // given
        SeparateQueryFromModifierExample separateQueryFromModifierExample = new SeparateQueryFromModifierExample();
        // when
        String[] people = {"tom", "nick"};
        separateQueryFromModifierExample.findMiscreant(people);
        String miscreant = separateQueryFromModifierExample.findPeople(people);
        // then
        assertThat(miscreant).isEqualTo("tom");
    }

    @Test
    public void should_return_empty_when_findMiscreant_given_people_not_contains_tom_and_sam() {
        // given
        SeparateQueryFromModifierExample separateQueryFromModifierExample = new SeparateQueryFromModifierExample();
        // when
        String[] people = {"tim", "nick"};
        separateQueryFromModifierExample.findMiscreant(people);
        String miscreant = separateQueryFromModifierExample.findPeople(people);
        // then
        assertThat(miscreant).isEqualTo("");
    }
}
