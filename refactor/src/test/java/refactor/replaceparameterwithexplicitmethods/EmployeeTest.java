package refactor.replaceparameterwithexplicitmethods;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeTest {
    @Test
    void should_return_manager_instance_when_create_given_manager() {
        // given
        // when
        Employee tom = Employee.createManager();
        // then
        assertThat(tom).isNotNull();
        assertThat(tom).isInstanceOf(Manager.class);
    }

    @Test
    void should_return_salesman_instance_when_create_given_salesman() {
        // given
        // when
        Employee tom = Employee.createSalesman();
        // then
        assertThat(tom).isNotNull();
        assertThat(tom).isInstanceOf(Salesman.class);
    }

    @Test
    void should_return_engineer_instance_when_create_given_engineer() {
        // given
        // when
        Employee tom = Employee.createEngineer();
        // then
        assertThat(tom).isNotNull();
        assertThat(tom).isInstanceOf(Engineer.class);
    }
}