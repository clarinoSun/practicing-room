package refactor.movemethod;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {

    @Test
    public void should_return_overdraftCharge_when_calcOverdraftCharge_given_account_type_premium_daysOverdrawn_bigger_than_7() {
        // given
        Account account = new Account(new AccountType(AccountType.ACCOUNT_TYPE_PREMIUM), 10);
        // when
        final double result = account.calcOverdraftCharge();
        // then
        assertThat(result).isEqualTo(12.55);
    }


    @Test
    public void should_return_overdraftCharge_when_calcOverdraftCharge_given_account_type_not_premium() {
        // given
        Account account = new Account(new AccountType(AccountType.ACCOUNT_TYPE_SENIOR), 10);
        // when
        final double result = account.calcOverdraftCharge();
        // then
        assertThat(result).isEqualTo(17.5);
    }


    @Test
    public void should_return_bankCharge_when_calcBankCharge_given_bigger_than_0() {
        // given
        Account account = new Account(new AccountType(AccountType.ACCOUNT_TYPE_SENIOR), 10);
        // when
        final double result = account.calcBankCharge();
        // then
        assertThat(result).isEqualTo(22);
    }
}