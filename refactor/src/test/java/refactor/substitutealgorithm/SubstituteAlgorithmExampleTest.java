package refactor.substitutealgorithm;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SubstituteAlgorithmExampleTest {

    private final SubstituteAlgorithmExample substituteAlgorithmExample = new SubstituteAlgorithmExample();

    @Test
    public void should_return_Tom_when_findPerson_given_people_array_contains_Tom() {
        // given
        String[] people = new String[]{"Tom", "Sam", "Tim"};
        // when
        final String result = substituteAlgorithmExample.findPerson(people);
        // then
        assertThat(result).isEqualTo("Tom");
    }


    @Test
    public void should_return_Sam_when_findPerson_given_people_array_not_contains_Tom_but_contains_Sam() {
        // given
        String[] people = new String[]{"Sam", "Tim"};
        // when
        final String result = substituteAlgorithmExample.findPerson(people);
        // then
        assertThat(result).isEqualTo("Sam");
    }


    @Test
    public void should_return_Tim_when_findPerson_given_people_array_not_contains_Tom_and_Sam_but_contains_Tim() {
        // given
        String[] people = new String[]{"Tim"};
        // when
        final String result = substituteAlgorithmExample.findPerson(people);
        // then
        assertThat(result).isEqualTo("Tim");
    }


    @Test
    public void should_return_Bob_when_findPerson_given_people_array_not_contains_Tom_and_Sam_and_Tim_but_contains_Bob() {
        // given
        String[] people = new String[]{"Bob"};
        // when
        final String result = substituteAlgorithmExample.findPerson(people);
        // then
        assertThat(result).isEqualTo("Bob");
    }

    @Test
    public void should_return_empty_String_when_findPerson_given_people_array_not_contains_Tom_and_Sam_and_Tim() {
        // given
        String[] people = new String[]{"Dayna"};
        // when
        final String result = substituteAlgorithmExample.findPerson(people);
        // then
        assertThat(result).isEqualTo("");
    }
}