

package refactor.refactorfourstpes;

import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.assertj.core.api.Assertions.assertThat;


class CustomerTest {
    Customer customer;

    @Test
    public void should_return_rental_record_when_state_given_movie_regular_daysRented_more_than_2_and_movie_release_daysRented_more_than_1_and_movie_children_daysRented_more_than_3() {
        // given
        final Vector rentals = new Vector();
        final Movie Movie_REGULAR_1 = new Movie(Movie.REGULAR, "常规片1");
        rentals.add(new Rental(Movie_REGULAR_1, 4));
        final Movie MOVIE_NEW_RELEASE_1 = new Movie(Movie.NEW_RELEASE, "新发片1");
        rentals.add(new Rental(MOVIE_NEW_RELEASE_1, 2));
        final Movie MOVIE_CHILDREN_1 = new Movie(Movie.CHILDREN, "儿童片1");
        rentals.add(new Rental(MOVIE_CHILDREN_1, 5));
        customer = new Customer(rentals, "Dayna");
        // when
        final String result = customer.state();
        // then
        assertThat(result).as("客户租赁情况").isEqualTo("Rental Record for Dayna\n" + "\t" + Movie_REGULAR_1.getTitle() + "\t5.0\n" +
                "\t" + MOVIE_NEW_RELEASE_1.getTitle() + "\t6.0\n" +
                "\t" + MOVIE_CHILDREN_1.getTitle() + "\t4.5\n" +
                "Amount owed is 15.5\nYou earned 4 frequent renter points"
        );
    }

}