package refactor.replaceconditionalwithpolymorphism;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeTest {
    @Test
    void should_return_monthlySalary_when_calcPayAmount_given_engineer() {
        // given
        Employee employee = new Employee(new Engineer(), 30000, 10000, 50000);
        // when
        int payAmount = employee.calcPayAmount();
        // then
        assertThat(payAmount).isEqualTo(30000);
    }

    @Test
    void should_return_monthlySalary_plus_commission_when_calcPayAmount_given_salesman() {
        // given
        Employee employee = new Employee(new Salesman(), 30000, 10000, 50000);
        // when
        int payAmount = employee.calcPayAmount();
        // then
        assertThat(payAmount).isEqualTo(40000);
    }

    @Test
    void should_return_monthlySalary_plus_bonus_when_calcPayAmount_given_manager() {
        // given
        Employee employee = new Employee(new Manager(), 30000, 10000, 50000);
        // when
        int payAmount = employee.calcPayAmount();
        // then
        assertThat(payAmount).isEqualTo(80000);
    }

}