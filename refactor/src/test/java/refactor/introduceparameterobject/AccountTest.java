package refactor.introduceparameterobject;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {
    @Test
    void should_return_flow_when_getFlowBetween_given_start_and_end() {
        // given
        Account account = new Account();
        account.setEntries(Lists.newArrayList(new Entry(new Date(2022, 7, 15), 200),
                new Entry(new Date(2022, 7, 16), 300),
                new Entry(new Date(2022, 7, 17), 400)));
        // when
        double flowBetween = account.getFlowBetween(new DateRange(new Date(2022, 7, 15), new Date(2022, 7, 16)));
        // then
        assertThat(flowBetween).isEqualTo(500.0);
    }
}