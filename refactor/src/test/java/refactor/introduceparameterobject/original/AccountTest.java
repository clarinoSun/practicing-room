package refactor.introduceparameterobject.original;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import refactor.introduceparameterobject.Account;
import refactor.introduceparameterobject.DateRange;
import refactor.introduceparameterobject.Entry;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {
    @Test
    void should_return_flow_when_getFlowBetween_given_start_and_end() {
        // given
        refactor.introduceparameterobject.Account account = new Account();
        account.setEntries(Lists.newArrayList(new refactor.introduceparameterobject.Entry(new Date(2022, 7, 15), 200),
                new refactor.introduceparameterobject.Entry(new Date(2022, 7, 16), 300),
                new Entry(new Date(2022, 7, 17), 400)));
        // when
        double flowBetween = account.getFlowBetween(new DateRange(new Date(2022, 7, 15), new Date(2022, 7, 16)));
        // then
        assertThat(flowBetween).isEqualTo(500.0);
    }
}