package refactor.introduceforeignmethod;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

class IntroduceForeignMethodExampleTest {
    private IntroduceForeignMethodExample introduceForeignMethodExample;

    @BeforeEach
    void setUp() {
        introduceForeignMethodExample = new IntroduceForeignMethodExample();
    }

    @Test
    public void should_return_20_when_calcPrice_given_previousDate_after_19901008() {
        // given
        Date previousDate = new Date(1990, 10, 9);
        // when
        final double price = introduceForeignMethodExample.calcPrice(previousDate);
        // then
        assertThat(price).isEqualTo(20.0);
    }


    @Test
    public void should_return_10_when_calcPrice_given_previousDate_before_19901008() {
        // given
        Date previousDate = new Date(1990, 10, 7);
        // when
        final double price = introduceForeignMethodExample.calcPrice(previousDate);
        // then
        assertThat(price).isEqualTo(10.0);
    }
}