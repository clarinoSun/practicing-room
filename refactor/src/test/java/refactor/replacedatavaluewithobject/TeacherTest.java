package refactor.replacedatavaluewithobject;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TeacherTest {
    @Test
    public void should_return_true_when_isTelephoneCorrect_given_TelephoneAreaCode_010() {
        // given
        Teacher teacher = new Teacher("dayna");
        teacher.setTelephone("010-88888888");
        // when
        boolean isTelephoneCorrect = teacher.isTelephoneCorrect();
        // then
        assertThat(isTelephoneCorrect).isTrue();
    }


    @Test
    public void should_return_false_when_isTelephoneCorrect_given_TelephoneAreaCode_not_010() {
        // given
        Teacher teacher = new Teacher("dayna");
        teacher.setTelephone("011-88888888");
        // when
        boolean isTelephoneCorrect = teacher.isTelephoneCorrect();
        // then
        assertThat(isTelephoneCorrect).isFalse();
    }
}