package refactor.movefield;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {
    @Test
    public void should_return_interest_when_calcInterest_given_accountType_premium() {
        // given
        final Account account = new Account(new AccountType(AccountType.ACCOUNT_TYPE_PREMIUM));
        account.setInterestRate(0.5);
        // when
        final BigDecimal result = account.calcInterest(200, 220);
        // then
        assertThat(result.doubleValue()).isEqualTo(60.27);
    }

    @Test
    public void should_return_interest_when_calcInterest_given_accountType_not_premium() {
        // given
        final Account account = new Account(new AccountType(AccountType.ACCOUNT_TYPE_SENIOR));
        account.setInterestRate(0.5);
        // when
        final BigDecimal result = account.calcInterest(200, 220);
        // then
        assertThat(result.doubleValue()).isEqualTo(61.11);
    }
}