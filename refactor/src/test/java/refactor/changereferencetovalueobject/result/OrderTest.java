package refactor.changereferencetovalueobject.result;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrderTest {


    @Test
    public void should_historyOrderStatement_not_be_changed_when_state_given_historyOrderAddress_changed() {
        // given
        Order historyOrder = new Order("1");
        Address oldCompanyAddress = new Address("company 17 floor");
        Address companyAddress = oldCompanyAddress;
        historyOrder.setAddress(companyAddress);
//        companyAddress.setAddress("company 18 floor");
        Address newCompanyAddress = new Address("company 18 floor");
        Order newOrder = new Order("2");
        newOrder.setAddress(newCompanyAddress);
        // when
        String historyOrderStatement = historyOrder.state();
        String newOrderStatement = newOrder.state();
        // then
        assertThat(historyOrderStatement).contains("company 17 floor");
        assertThat(newOrderStatement).contains("company 18 floor");
    }


}