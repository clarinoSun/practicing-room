package refactor.changereferencetovalueobject.original;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrderTest {
    @Test
    public void should_historyOrderStatement_be_changed_when_state_given_historyOrderAddress_changed() {
        // given
        Order historyOrder = new Order("1");
        Address oldCompanyAddress = Address.get("company 17 floor");
        Address companyAddress = oldCompanyAddress;
        historyOrder.setAddress(companyAddress);
        companyAddress.setAddress("company 18 floor");
        // when
        String historyOrderStatement = historyOrder.state();
        // then
        assertThat(historyOrderStatement).contains("company 18 floor");
    }
}