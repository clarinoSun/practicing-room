package refactor.preservewholeobject;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RoomTest {
    @Test
    void should_return_false_when_withinPlan_given_DaysTempRangeLow_lower_than_PlanLow() {
        // given
        Room room = new Room();
        room.setDaysTempRange(new TempRange(10, 18));
        HeatingPlan plan = new HeatingPlan(new TempRange(15, 25));
        // when
        boolean result = room.withinPlan(plan);
        // then
        assertThat(result).isEqualTo(false);
    }

    @Test
    void should_return_true_when_withinPlan_given_DaysTempRange_within_Plan() {
        // given
        Room room = new Room();
        room.setDaysTempRange(new TempRange(16, 18));
        HeatingPlan plan = new HeatingPlan(new TempRange(15, 25));
        // when
        boolean result = room.withinPlan(plan);
        // then
        assertThat(result).isEqualTo(true);
    }

    @Test
    void should_return_false_when_withinPlan_given_DaysTempRangeHigh_higher_than_PlanHigh() {
        // given
        Room room = new Room();
        room.setDaysTempRange(new TempRange(16, 28));
        HeatingPlan plan = new HeatingPlan(new TempRange(15, 25));
        // when
        boolean result = room.withinPlan(plan);
        // then
        assertThat(result).isEqualTo(false);
    }
}