package refactor.replacearraywithobject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ReplaceArrayWithObjectExampleTest {
    private ReplaceArrayWithObjectExample replaceArrayWithObjectExample;

    @BeforeEach
    void setUp() {
        replaceArrayWithObjectExample = new ReplaceArrayWithObjectExample();
    }

    @Test
    public void should_stateTeamPerformance_when_stateTeamPerformance() {
        // given
        // when
        String statement = replaceArrayWithObjectExample.stateTeamPerformance();
        // then
        assertThat(statement).isEqualTo("name:Liverpool,wins:15");
    }
}