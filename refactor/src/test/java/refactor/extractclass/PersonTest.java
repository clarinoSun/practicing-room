package refactor.extractclass;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {
    @Test
    public void should_return_TelephoneNumber_when_getTelephoneNumber_given_person_with_officeAreaCode_and_officeNumber() {
        // given
        final Person person = new Person("Sam");
        person.getOfficeTelephone().setAreaCode("020");
        person.getOfficeTelephone().setNumber("88888888");
        // when
        final String telephoneNumber = person.getTelephoneNumber();
        // then
        assertThat(telephoneNumber).isEqualTo("(020)88888888");
    }
}