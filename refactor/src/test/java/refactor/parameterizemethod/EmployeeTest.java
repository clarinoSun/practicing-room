package refactor.parameterizemethod;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeTest {
    @Test
    void should_return_result_when_calcBaseCharge_given_lastUsage_bigger_than_200() {
        // given
        Employee employee = new Employee(30000);
        // when
        double result = employee.calcBaseCharge();
        // then
        assertThat(result).isEqualTo(1500);
    }
}