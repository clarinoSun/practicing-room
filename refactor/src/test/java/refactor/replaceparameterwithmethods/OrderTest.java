package refactor.replaceparameterwithmethods;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrderTest {
    @Test
    void should_return_price_when_calcPrice_given_quantity_bigger_than_100() {
        // given
        Order order = new Order(200, 1000);
        // when
        double result = order.calcPrice();
        // then
        assertThat(result).isEqualTo(20000.0);
    }


    @Test
    void should_return_price_when_calcPrice_given_quantity_smaller_than_100() {
        // given
        Order order = new Order(90, 1000);
        // when
        double result = order.calcPrice();
        // then
        assertThat(result).isEqualTo(4500.0);
    }
}